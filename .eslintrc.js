module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: "airbnb-base",
  plugins: ["babel"],
  overrides: [
    {
      env: {
        node: true,
      },
      files: [".eslintrc.{js,cjs}"],
      parserOptions: {
        sourceType: "script",
      },
    },
  ],
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module",
  },
  rules: {
    quotes: "off",
    "no-alert": "off",
    "linebreak-style": "off",
    "class-methods-use-this": "off",
    "func-names": "off",
    "no-param-reassign": "off",
    "no-restricted-syntax": "off",
    "no-shadow": "off",
    "max-classes-per-file": "off",
    "no-underscore-dangle": "off",
    "no-multi-assign": "off",
    "no-lonely-if": "off",
    "default-case": "warn",
    "import/extensions": ["error", "never"],
    "prefer-arrow-callback": "error",
    "no-alert": "error",
    "consistent-return": "error",
    "import/prefer-default-export": "warn",
    "no-plusplus": "error",
    "no-console": "warn",
    "eol-last": ["error", "always"],
    "no-use-before-define": "error",
    "operator-linebreak": ["error", "after"],
    "import/no-extraneous-dependencies": "error",
    "import/order": [
      "error",
      {
        groups: [
          "builtin",
          "external",
          "internal",
          "parent",
          "sibling",
          "index",
        ],
        "newlines-between": "always",
      },
    ],
    "quote-props": ["error", "as-needed"],
  },
};
