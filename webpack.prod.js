import * as path from "path";
import { merge } from "webpack-merge";
import TerserPlugin from "terser-webpack-plugin";
import { CleanWebpackPlugin } from "clean-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
import * as url from "url";

import { commonConfig } from "./webpack.common.js";

const __dirname = url.fileURLToPath(new URL(".", import.meta.url));

export default merge(commonConfig, {
  mode: "production",
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin()],
  },
  plugins: [new CleanWebpackPlugin(), new MiniCssExtractPlugin()],
    output: {
    /* eslint-disable no-undef */
    path: path.resolve(__dirname, "build"),
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
    ],
  },
});
