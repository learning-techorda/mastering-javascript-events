import { expect } from "chai";
import sinon from "sinon";
import { validate } from "../email-validator.js";
import { validateAsync } from "../email-validator.js";
import { validateWithThrow } from "../email-validator.js";
import { validateWithLog } from "../email-validator.js";

describe("first test", () => {
  it("should return 2", () => {
    expect(2).to.equal(2);
  });
});

describe("validate", () => {
  it("should return true for valid email which ends with @gmail.com ", () => {
    const email = "firsttest@gmail.com";
    expect(validate(email)).to.be.true;
  });
  it("should return true for valid email which ends with @outlook.com", () => {
    const email = "secondtest@outlook.com";
    expect(validate(email)).to.be.true;
  });
  it("should return true for valid email which ends with @yandex.ru", () => {
    const email = "thirdtest@yandex.ru";
    expect(validate(email)).to.be.true;
  });
  it("should return false for valid email addresses with invalid domain @mail.ru", () => {
    const email = "fourthtest@mail.ru";
    expect(validate(email)).to.be.false;
  });
  it("should return false for valid email addresses with invalid domain @gmail.ua", () => {
    const email = "fifthtest@gmail.ua";
    expect(validate(email)).to.be.false;
  });
  it("should return false for valid email addresses with invalid domain @outlook.info", () => {
    const email = "sixthtest@outlook.info";
    expect(validate(email)).to.be.false;
  });
  it("should return false for valid email addresses with invalid domain @yandex.org", () => {
    const email = "seventhtest@yandex.org";
    expect(validate(email)).to.be.false;
  });
  it("should return false for input data that is a set of unrelated numbers", () => {
    const email = "123456789";
    expect(validate(email)).to.be.false;
  });
  it("should return false for input data that is a set of unrelated letters", () => {
    const email = "sdfghjk";
    expect(validate(email)).to.be.false;
  });
});

describe("validateAsync", () => {
  it("should resolve true asynchronously if the email contains a valid ending", async () => {
    const result = await validateAsync("firsttest@gmail.com");
    expect(result).to.be.true;
  });
  it("should resolve false if the email invalid", async () => {
    const result = await validateAsync("secondtest@mail.ru");
    expect(result).to.be.false;
  });
});

describe("validateWithThrow", () => {
  it("should return true if the email contains a valid ending", () => {
    const result = validateWithThrow("firsttest@gmail.com");
    expect(result).to.be.true;
  });
  it("should throw an error message about an invalid email", () => {
    expect(() => validateWithThrow("secondtest@mail.ru")).to.throw(
      "Provided email is invalid."
    );
  });
});

describe("validateWithLog", () => {
  let consoleSpy;

  beforeEach(() => {
    consoleSpy = sinon.spy(console, "log");
  });

  afterEach(() => {
    consoleSpy.restore();
  });

  it("should log the valid email which ends with @gmail.com", () => {
    const email = "firsttest@gmail.com";
    validateWithLog(email);
    expect(consoleSpy.calledOnce).to.be.true;
    expect(
      consoleSpy.calledWith(`should return true for valid email ${email}: true`)
    ).to.be.true;
  });
  it("should log the valid email which ends with @outlook.com", () => {
    const email = "secondtest@outlook.com";
    validateWithLog(email);
    expect(consoleSpy.calledOnce).to.be.true;
    expect(
      consoleSpy.calledWith(`should return true for valid email ${email}: true`)
    ).to.be.true;
  });
  it("should log the valid email which ends with @yandex.ru", () => {
    const email = "thirdtest@yandex.ru";
    validateWithLog(email);
    expect(consoleSpy.calledOnce).to.be.true;
    expect(
      consoleSpy.calledWith(`should return true for valid email ${email}: true`)
    ).to.be.true;
  });
  it("should log the validation result for an invalid email with invalid domain @mail.ru", () => {
    const email = "fourthtest@mail.ru";
    validateWithLog(email);
    expect(
      consoleSpy.calledWith(
        `should return false for invalid email ${email}: false`
      )
    ).to.be.false;
  });
  it("should log the validation result for an invalid email with invalid domain @gmail.ua", () => {
    const email = "fifthtest@gmail.ua";
    validateWithLog(email);
    expect(
      consoleSpy.calledWith(
        `should return false for invalid email ${email}: false`
      )
    ).to.be.false;
  });
  it("should log the validation result for an invalid email with invalid domain @outlook.info", () => {
    const email = "sixthtest@outlook.info";
    validateWithLog(email);
    expect(
      consoleSpy.calledWith(
        `should return false for invalid email ${email}: false`
      )
    ).to.be.false;
  });
  it("should log the validation result for an invalid email with invalid domain @yandex.org", () => {
    const email = "seventhtest@yandex.org";
    validateWithLog(email);
    expect(
      consoleSpy.calledWith(
        `should return false for invalid email ${email}: false`
      )
    ).to.be.false;
  });
  it("should log for input data that is a set of unrelated numbers ", () => {
    const email = "123456789";
    validateWithLog(email);
    expect(
      consoleSpy.calledWith(
        `should return false for invalid email ${email}: false`
      )
    ).to.be.false;
  });
  it("should log for input data that is a set of unrelated letters ", () => {
    const email = "sdfghjk";
    validateWithLog(email);
    expect(
      consoleSpy.calledWith(
        `should return false for invalid email ${email}: false`
      )
    ).to.be.false;
  });
});
