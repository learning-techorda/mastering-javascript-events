/* eslint-disable-next-line max-classes-per-file */
class SectionCreator {
  /* eslint-disable default-case */
  /* eslint-disable consistent-return */
  /* eslint-disable no-use-before-define */
  create(type) {
    switch (type) {
      case "standard":
        return new StandardSection();
      case "advanced":
        return new AdvancedSection();
    }
  }
}
class StandardSection {
  constructor() {
    this.element = document.createElement("section");
    this.element.className = "join-our-program-section";

    const websiteSection = document.createElement("website-section");
    websiteSection.setAttribute("title-text", "Join Our Program");
    websiteSection.setAttribute(
      "subtitle-text",
      "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    );
    websiteSection.setAttribute("title-class", "join-our-program-heading");
    websiteSection.setAttribute(
      "subtitle-class",
      "join-our-program-description"
    );

    const form = document.createElement("form");
    form.slot = "content";
    form.id = "join-our-program-form";
    form.className = "join-our-program-form";

    const inputEmail = document.createElement("input");
    inputEmail.className = "join-our-program-input";
    inputEmail.type = "email";
    inputEmail.placeholder = "Email";

    const subscribeButton = document.createElement("button");
    subscribeButton.className = "join-our-program-submit";
    subscribeButton.textContent = "Subscribe";

    form.appendChild(inputEmail);
    form.appendChild(subscribeButton);

    this.element.appendChild(websiteSection);
    this.element.appendChild(form);
  }

  remove() {
    this.element.remove();
  }
}

class AdvancedSection {
  constructor() {
    this.element = document.createElement("section");
    this.element.className = "join-our-program-section";

    const heading = document.createElement("h2");
    heading.className = "join-our-program-heading";
    heading.textContent = "Join Our Advanced Program";

    const description = document.createElement("p");
    description.className = "join-our-program-description";
    description.textContent =
      "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";

    const form = document.createElement("form");
    form.className = "join-our-program-form";

    const inputEmail = document.createElement("input");
    inputEmail.className = "join-our-program-input";
    inputEmail.type = "email";
    inputEmail.placeholder = "Email";

    const subscribeButton = document.createElement("button");
    subscribeButton.className = "join-our-program-submit";
    subscribeButton.textContent = "Subscribe to Advanced Program";

    form.appendChild(inputEmail);
    form.appendChild(subscribeButton);

    this.element.appendChild(heading);
    this.element.appendChild(description);
    this.element.appendChild(form);
  }

  remove() {
    this.element.remove();
  }
}

export default SectionCreator;
