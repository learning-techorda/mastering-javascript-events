class WebsiteSection extends HTMLElement {
  constructor() {
    super();
  }
  connectedCallback() {
    const shadowRoot = this.attachShadow({ mode: "open" });
    const sectionClass = this.getAttribute("section-class");
    const titleText = this.getAttribute("title-text");
    const subtitleText = this.getAttribute("subtitle-text");
    const titleClass = this.getAttribute("title-class");
    const subtitleClass = this.getAttribute("subtitle-class");

    shadowRoot.innerHTML = `
        <style>
        @import "./styles/style.css";
        @import "./styles/main.css";
        @import "./styles/big-community.css";
        @import "./styles/normalize.css";
        </style>
        <section class="${sectionClass}">
        <slot name="image"></slot>
        <h1 class="${
          this.getAttribute("title-class")
            ? this.getAttribute("title-class")
            : "hidden"
        }">${this.getAttribute("title-text")}</h1>
        <p class="${
          this.getAttribute("subtitle-class")
            ? this.getAttribute("subtitle-class")
            : "hidden"
        }">${this.getAttribute("subtitle-text")}</p>           
        <slot name="content"></slot>
        <slot name="h1"></slot>
        <slot name="button"></slot>
        <slot name="p"></slot>
        </section>
        `;
  }
}
customElements.define("website-section", WebsiteSection);
