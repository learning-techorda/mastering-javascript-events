let items = [];

self.onmessage = function (event) {
  items.push(event.data);
  if (items.length === 5) {
    fetch("http://localhost:3000/analytics/user", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(items),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("server:", data);
        items = [];
      })
      .catch((error) => console.error("Error:", error));
  }
};
