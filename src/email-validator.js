function validate(email) {
  const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com", "yandex.ru"];
  /* eslint-disable no-plusplus */
  for (let i = 0; i < VALID_EMAIL_ENDINGS.length; i++) {
    if (email.endsWith(VALID_EMAIL_ENDINGS[i])) {
      return true;
    }
  }
  return false;
}
function validateAsync(email) {
  return new Promise((resolve) => {
    const isValid = validate(email);
    resolve(isValid);
  });
}

function validateWithThrow(email) {
  const isValid = validate(email);
  if (!isValid) {
    throw new Error("Provided email is invalid.");
  }
  return true;
}

function validateWithLog(email) {
  const isValid = validate(email);
  console.log(`should return true for valid email ${email}: ${isValid}`);
  return isValid;
}
/* eslint-disable import/prefer-default-export */
/* eslint-disable eol-last */
export { validate, validateAsync, validateWithThrow, validateWithLog };
