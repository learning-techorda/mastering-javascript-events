const API_URL = process.env.API_URL;

function sendData(communityFetchTime) {
  const navigationEntry = performance.getEntriesByType("navigation")[0];

  const measures = {
    communityFetchTime,
    pageLoadTime: navigationEntry.loadEventEnd - navigationEntry.startTime,
    usedJSHeapSize: performance.memory.usedJSHeapSize,
    totalJSHeapSize: performance.memory.totalJSHeapSize,
    jsHeapSizeLimit: performance.memory.jsHeapSizeLimit,
  };

  fetch(`${API_URL}/analytics/performance`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify([measures]),
  })
    .then((response) => response.json())
    .then((result) => console.log("success", result))
    .catch((error) => console.error("error", error));
}

export { sendData };
