/* eslint-disable import/extensions */
/* eslint-disable no-alert */
import SectionCreator from "./join-us-section-copy.js";
import { validate } from "./email-validator.js";
import { sendData } from "./performance.js";

import "./styles/style.css";
import "./styles/main.css";
import "./styles/big-community.css";

const API_URL = process.env.API_URL;
// add worker path
const worker = new Worker("./web-worker.js");

window.addEventListener("load", () => {
  const sectionContainer = document.getElementById("join-our-program");

  const creator = new SectionCreator();
  console.log("load join us");
  const section = creator.create("standard");
  sectionContainer.appendChild(section.element);
  /* eslint-disable prefer-arrow-callback */
  const form = section.element.querySelector("form");
  const inputEmail = form.querySelector(".join-our-program-input");
  const subscribeButton = section.element.querySelector(
    ".join-our-program-submit"
  );

  function subscriptionUpdate() {
    const Subscribed = localStorage.getItem("Subscribed") === "true";
    const storedEmail = localStorage.getItem("subscriptionEmail");

    if (Subscribed) {
      inputEmail.classList.add("hidden");
      subscribeButton.textContent = "Unsubscribe";
    } else {
      inputEmail.classList.remove("hidden");
      subscribeButton.textContent = "Subscribe";
      if (storedEmail && validate(storedEmail)) {
        inputEmail.value = storedEmail;
      } else {
        inputEmail.value = "";
      }
    }
  }
  subscriptionUpdate();

  form.addEventListener("submit", function (event) {
    event.preventDefault();
    if (validate(inputEmail.value)) {
      inputEmail.classList.add("hidden");
      subscribeButton.textContent = "Unsubscribe";
      localStorage.setItem("Subscribed", "true");
      localStorage.setItem("subscriptionEmail", inputEmail.value);
    }
  });
  // input-worker
  inputEmail.addEventListener("click", () => {
    const userEndpoint = {
      timestamp: new Date().getTime(),
      data: [],
    };
    worker.postMessage(userEndpoint);
  });
  // Disable
  function setButtonDisabledState(button, isDisabled) {
    button.disabled = isDisabled;
    button.style.opacity = isDisabled ? 0.5 : 1;
  }

  // ПОДПИСАТЬСЯ
  function subscribe(subscribedEmail) {
    return fetch(`${API_URL}/subscribe`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email: subscribedEmail }),
    }).then((response) => response.json());
  }

  // ОТПИСАТЬСЯ
  function unsubscribe(subscribedEmail) {
    return fetch(`${API_URL}/unsubscribe`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email: subscribedEmail }),
    }).then((response) => response.json());
  }

  subscribeButton.addEventListener("click", () => {
    const isCurrentlySubscribed = localStorage.getItem("Subscribed") === "true";
    const subscribedEmail = localStorage.getItem("subscriptionEmail");

    // add worker unsubscribe
    const userEndpoint = {
      timestamp: new Date().getTime(),
      data: [],
    };
    worker.postMessage(userEndpoint);

    if (isCurrentlySubscribed) {
      setButtonDisabledState(subscribeButton, true);

      unsubscribe(subscribedEmail)
        .then((res) => {
          if (res.success) {
            inputEmail.classList.remove("hidden");
            subscribeButton.textContent = "Subscribe";
            localStorage.removeItem("subscriptionEmail");
            localStorage.setItem("Subscribed", "false");
            inputEmail.value = "";
          } else {
            window.alert(res.error);
          }
        })
        .catch((error) => {
          window.alert(`Server error: ${error.message}`);
        })
        .finally(() => {
          setButtonDisabledState(subscribeButton, false);
        });
    } else {
      if (validate(inputEmail.value)) {
        setButtonDisabledState(subscribeButton, true);

        subscribe(inputEmail.value)
          .then((res) => {
            if (res.success) {
              inputEmail.classList.add("hidden");
              subscribeButton.textContent = "Unsubscribe";
              localStorage.setItem("Subscribed", "true");
              localStorage.setItem("subscriptionEmail", inputEmail.value);
              // add worker subscribe
              const userEndpoint = {
                timestamp: new Date().getTime(),
                data: [],
              };
              worker.postMessage(userEndpoint);
            } else {
              window.alert(res.error);
            }
          })
          .catch((error) => {
            window.alert(`Server error: ${error.message}`);
          })
          .finally(() => {
            setButtonDisabledState(subscribeButton, false);
          });
      }
    }
  });
});

// Big Community of People Like You
window.addEventListener("load", () => {
  const sectionContainer = document.querySelector(
    ".big-community-of-people-like-you"
  );

  sectionContainer.innerHTML = `
  <div class ="community-container">
      <website-section
        title-text="Big Community of People Like You"
        subtitle-text="We’re proud of our products, and we’re really excited when we get feedback from our users."
        title-class="community-heading"
        subtitle-class="par"></website-section>
      <article slot="content" class="humansContainer">
        <div class="humansContainer"></div>
      </article>
      </div>
  `;

  const humanList = sectionContainer.querySelector(".humansContainer");
  function newHuman(
    avatar,
    personName,
    personDescription,
    position,
    paddingBottomClass,
    positionWidth
  ) {
    const human = `
      <div class="personContainer">
        <img src="${avatar}" alt="${personName}">
        <p class="description ${paddingBottomClass}">${personDescription}</p>
        <strong>${personName}</strong>
        <p class="position ${positionWidth}">${position}</p>
      </div>
    `;
    humanList.innerHTML += human;
  }
  const descriptions = [
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.",
    "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.",
    "Aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
  ];
  const paddingBottomClasses = [
    "description-padding-1",
    "description-padding-2",
    "description-padding-3",
  ];
  const positionWidths = [
    "position-width-90",
    "position-width-85",
    "position-width-90",
  ];

  const startTime = performance.now();

  fetch(`${API_URL}/community`)
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      return response.json();
    })
    .then((humans) => {
      humans.forEach((human, index) => {
        const description = descriptions[index];
        const paddingBottomClass = paddingBottomClasses[index];
        const positionWidth = positionWidths[index];

        newHuman(
          human.avatar,
          `${human.firstName} ${human.lastName}`,
          description,
          human.position,
          paddingBottomClass,
          positionWidth
        );
      });

      const endTime = performance.now();
      sendData(endTime - startTime);
    })
    .catch((error) => {
      console.error(`Server error: ${error.message}`);
    });
});
