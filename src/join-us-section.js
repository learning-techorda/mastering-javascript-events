class SectionCreator {
  create(type) {
    const section = document.createElement("section");
    section.className = "join-our-program-section";

    const heading = document.createElement("h2");
    heading.className = "join-our-program-heading";
    heading.textContent =
      type === "advanced" ? "Join Our Advanced Program" : "Join Our Program";

    const description = document.createElement("p");
    description.className = "join-our-program-description";
    description.textContent =
      "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";

    const form = document.createElement("form");
    form.className = "join-our-program-form";

    const inputEmail = document.createElement("input");
    inputEmail.className = "join-our-program-input";
    inputEmail.type = "email";
    inputEmail.placeholder = "Email";

    const subscribeButton = document.createElement("button");
    subscribeButton.className = "join-our-program-submit";
    subscribeButton.textContent =
      type === "advanced" ? "Subscribe to Advanced Program" : "SUBSCRIBE";

    form.appendChild(inputEmail);
    form.appendChild(subscribeButton);

    section.appendChild(heading);
    section.appendChild(description);
    section.appendChild(form);

    return {
      element: section,
      remove() {
        section.remove();
      },
    };
  }
}
export default SectionCreator;
