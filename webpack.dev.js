/* eslint-disable import/order */
import * as path from "path";
import * as url from "url";
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable import/extensions */
import { merge } from "webpack-merge";
import { commonConfig } from "./webpack.common.js";

const __dirname = url.fileURLToPath(new URL(".", import.meta.url));

export default merge(commonConfig, {
  mode: "development",
  devtool: "inline-source-map",
  devServer: {
    static: path.join(__dirname, "src"),
    open: true,
    port: 9000,
    proxy: [
      {
        context: ["/api"],
        target: "http://localhost:3000",
        pathRewrite: { "^/api": "" },
      },
    ],
  },
});
